import utils
import random

pattern_size = 4000
width = 2
height = 2

print(utils.select_pen(1))

drawing = 'PU;'

pattern_template_list = list(utils.pattern_templates.values())

# Make first layer of patterns
for row in range(height):
  for column in range(width):
    # empty slot 1 in four times
    if random.randint(0,4) < 4:
      drawing += utils.move_to(column * pattern_size, row * pattern_size)
      drawing += utils.render_pattern(random.choice(pattern_template_list), pattern_size) + 'PM1;'

# Second layer shifted half pattern size
for row in range(height-1):
  for column in range(width-1):
    # empty slot 1 in three times
    if random.randint(0,3) < 3:
      drawing += utils.move_to(column * pattern_size + .5 * pattern_size, row * pattern_size * pattern_size + .5 * pattern_size)
      drawing += utils.render_pattern(random.choice(pattern_template_list), pattern_size) + 'PM1;'

# combine all in polygon
print(utils.make_polygon(drawing))
print(utils.set_fill(150, random.randint(4,6) * 5))
print(utils.edge())
print(utils.fill())