import random

# Dict of patterns
# {full} → full pattern size
# {threequarter} → threequarter pattern size
# {half} → half pattern size
# {quarter} → quarter pattern size
pattern_templates = {
  'circle-with-gap': 'PU;PR{half},{half};PD;CI{half};CI{quarter};PM1;PU;PR-{half},-{half};',
  'two-squares': 'PD;PR{half},0,0,{full},{half},0,0,-{half},-{full},0,0,-{half};PM1;PU;',
  'square-with-gap': 'PD;PR{full},0,0,{full},-{full},0,0,-{full};PU;PR{quarter},{quarter};PD;PR{half},0,0,{half},-{half},0,0,-{half};PM1;PU;PR-{quarter},-{quarter};',
  'two-triangles': 'PD;PR{full},0,-{full},{full},{full},0,-{full},-{full};PM1;PU;',
  'four-circles': 'PU;PR{quarter},{quarter};CI{quarter};PU;PR0,{half};CI{quarter};PU;PR{half},0;CI{quarter};PU;PR0,-{half};CI{quarter};PM1;PU;PR-{threequarter},-{quarter};',
  'four-triangles': 'PD;PR{full},0,0,{full},-{full},0,0,-{full};PM1;PU;PR0,{half};PD;PR{half},{half},{half},-{half},-{half},-{half},-{half},{half};PM1;PU;PR0,-{half};'
}


def get_x_random_pattern_templates (pattern_templates, amount=1):
  templates = list(pattern_templates.values())
  random.shuffle(templates)
  return templates[:amount]

# Render HPGL for a given template at a given size
def render_pattern (template, size):
  full = size
  threequarter = int(.75 * size)
  half = int(.5 * size)
  quarter = int(.25 * size)

  return template.format(full=full, threequarter=threequarter, half=half, quarter=quarter)

# Stack patterns. 
def combine (hpgl_pattern_strings):
  return ';'.join(hpgl_pattern_strings)

def make_polygon (hpgl):
  return 'PM0;{}PM2;'.format(hpgl)

# Return HPGL to draw edge of polygon
def edge ():
  return 'EP;'

# Return HPGL to fill polygon
def fill ():
  return 'FP;'

# Return HPGL to set fill type
def set_fill (interval, angle):
  return 'FT3,{}, {};'.format(interval, angle)

# Return HPGL to select pen
def select_pen (pen):
  return 'SP{};'.format(pen)

# Return HPGL to move pen to given x, y
def move_to (x, y):
  return 'PA{},{};'.format(x,y)
