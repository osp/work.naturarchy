import utils
import random

amount_of_patterns_per_cell = 3
pattern_size = 4000
width = 3
height = 5

print(utils.select_pen(1))

for row in range(height):
  for column in range(width):
    print(utils.move_to(column * pattern_size, row * pattern_size))
    patterns = utils.get_x_random_pattern_templates(utils.pattern_templates, amount_of_patterns_per_cell)
    rendered_patterns = [utils.render_pattern(pattern, pattern_size) for pattern in patterns]
    combined = utils.combine(rendered_patterns)
    print(utils.make_polygon(combined))
    print(utils.set_fill(150, random.randint(4,6) * 5))
    print(utils.fill())
    print(utils.set_fill(150, random.randint(4,6) * 5))
    print(utils.fill())
    print(utils.edge())